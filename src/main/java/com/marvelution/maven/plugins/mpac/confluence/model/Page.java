/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.confluence.model;

import com.atlassian.marketplace.client.model.Links;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Page entity
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public final class Page {

	@JsonProperty
	private Links links;
	@JsonProperty
	private String type;
	@JsonProperty
	private Long id;
	@JsonProperty
	private String title;
	@JsonProperty
	private String wikiLink;
	@JsonProperty
	private PageBody body;

	@JsonCreator
	public Page(@JsonProperty("body") PageBody body, @JsonProperty("id") Long id, @JsonProperty("link") Links links,
	            @JsonProperty("title") String title, @JsonProperty("type") String type, @JsonProperty("wikiLink") String wikiLink) {
		this.body = body;
		this.id = id;
		this.links = links;
		this.title = title;
		this.type = type;
		this.wikiLink = wikiLink;
	}

	public PageBody getBody() {
		return body;
	}

	public Long getId() {
		return id;
	}

	public Links getLinks() {
		return links;
	}

	public String getTitle() {
		return title;
	}

	public String getType() {
		return type;
	}

	public String getWikiLink() {
		return wikiLink;
	}

}
