/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.confluence.model;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Page body entity
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public final class PageBody {

	@JsonProperty
	private int type;
	@JsonProperty
	private String value;

	@JsonCreator
	public PageBody(@JsonProperty("value") String value, @JsonProperty("type") int type) {
		this.value = value;
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public int getType() {
		return type;
	}

}
