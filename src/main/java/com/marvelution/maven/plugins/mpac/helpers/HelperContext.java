/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.helpers;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Settings;

import java.io.File;

/**
 * Context for Helpers
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public class HelperContext {

	private Settings settings;
	private MavenSession session;
	private MavenProject project;
	private File basedir;

	public Settings getSettings() {
		return settings;
	}

	public MavenSession getSession() {
		return session;
	}

	public MavenProject getProject() {
		return project;
	}

	public File getBasedir() {
		return basedir;
	}

	public static Builder builder() {
		return new Builder();
	}

	/**
	 * Builder class for the HelperContext
	 */
	public static class Builder {
		private Settings settings;
		private MavenProject project;
		private MavenSession session;
		private File basedir;

		private Builder() {}

		public Builder project(MavenProject project) {
			this.project = project;
			return this;
		}

		public Builder session(MavenSession session) {
			this.session = session;
			return this;
		}

		public Builder settings(Settings settings) {
			this.settings = settings;
			return this;
		}

		public Builder basedir(File basedir) {
			this.basedir = basedir;
			return this;
		}

		public HelperContext build() {
			HelperContext context = new HelperContext();
			context.project = project;
			context.session = session;
			context.settings = settings;
			context.basedir = basedir;
			return context;
		}

	}

}
