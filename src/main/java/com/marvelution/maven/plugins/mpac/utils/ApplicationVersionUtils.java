/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.utils;

import com.atlassian.marketplace.client.model.Application;
import com.atlassian.marketplace.client.model.ApplicationVersion;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;
import com.google.common.primitives.Longs;
import org.apache.maven.artifact.versioning.ArtifactVersion;

import java.util.Comparator;
import java.util.List;

/**
 * Helper for {@link ApplicationVersion}s
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public final class ApplicationVersionUtils {

	/**
	 * Comparator to compare 2 {@link ApplicationVersion}s on there build numbers
	 */
	public static final Comparator<ApplicationVersion> APPLICATION_VERSION_COMPARATOR = new Comparator<ApplicationVersion>() {
		@Override
		public int compare(ApplicationVersion applicationVersion, ApplicationVersion applicationVersion2) {
			return Longs.compare(applicationVersion.getBuildNumber(), applicationVersion2.getBuildNumber());
		}
	};
	/**
	 * {@link Ordering} implementation that uses the {@link #APPLICATION_VERSION_COMPARATOR}
	 */
	public static final Ordering<ApplicationVersion> APPLICATION_VERSION_ORDERING = Ordering.from(APPLICATION_VERSION_COMPARATOR);

	/**
	 * Sort the {@link ApplicationVersion}s  of the given {@link Application}
	 *
	 * @param application the {@link Application} to sort the versions of
	 * @return the sorted {@link List} of {@link ApplicationVersion}s in ascending order
	 */
	public static List<ApplicationVersion> getSortedApplicationVersions(Application application) {
		return APPLICATION_VERSION_ORDERING.sortedCopy(application.getVersions());
	}

	/**
	 * Get the {@link ApplicationVersion} equivalent of the given {@link ArtifactVersion}
	 *
	 * @param applicationVersions the {@link List} of {@link ApplicationVersion}s to get the correct one from
	 * @param artifactVersion     the {@link ArtifactVersion} to get the {@link ApplicationVersion} equivalent for
	 * @return the {@link ApplicationVersion} equivalent of the {@link ArtifactVersion}, or {@code null} in case the version is not found
	 */
	public static ApplicationVersion getApplicationVersion(List<ApplicationVersion> applicationVersions,
	                                                       final ArtifactVersion artifactVersion) {
		return Iterables.find(applicationVersions, new Predicate<ApplicationVersion>() {
			@Override
			public boolean apply(ApplicationVersion applicationVersion) {
				return applicationVersion != null && applicationVersion.getVersion().equals(artifactVersion.toString());
			}
		}, null);
	}

	/**
	 * Get the previous version of the given {@link ApplicationVersion}
	 *
	 * @param applicationVersions the {@link List} of {@link ApplicationVersion} to get the previous version from
	 * @param version             the specific {@link ApplicationVersion} to get the previous version of
	 * @return the previous version, or {@code null}
	 */
	public static ApplicationVersion getPreviousApplicationVersion(List<ApplicationVersion> applicationVersions,
	                                                               ApplicationVersion version) {
		int index = applicationVersions.indexOf(version);
		if (index - 1 >= 0) {
			return applicationVersions.get(index - 1);
		}
		return null;
	}

	/**
	 * Get the next version of the given {@link ApplicationVersion}
	 *
	 * @param applicationVersions the {@link List} of {@link ApplicationVersion} to get the next version from
	 * @param version             the specific {@link ApplicationVersion} to get the next version of
	 * @return the next version, or {@code null}
	 */
	public static ApplicationVersion getNextApplicationVersion(List<ApplicationVersion> applicationVersions, ApplicationVersion version) {
		int index = applicationVersions.indexOf(version);
		if (index + 1 < applicationVersions.size()) {
			return applicationVersions.get(index + 1);
		}
		return null;
	}

	/**
	 * Check if applicationVersion2 is older then applicationVersion
	 *
	 * @param applicationVersion  {@link ApplicationVersion} to compare against
	 * @param applicationVersion2 {@link ApplicationVersion} to check
	 * @return {@code true} if the version is older, {@code false} otherwise
	 */
	public static boolean isOlder(ApplicationVersion applicationVersion, ApplicationVersion applicationVersion2) {
		return APPLICATION_VERSION_COMPARATOR.compare(applicationVersion2, applicationVersion) < 0;
	}

	/**
	 * Check if applicationVersion2 is older then applicationVersion
	 *
	 * @param applicationVersion  {@link ApplicationVersion} to compare against
	 * @param applicationVersion2 {@link ApplicationVersion} to check
	 * @return {@code true} if the version is newer, {@code false} otherwise
	 */
	public static boolean isNewer(ApplicationVersion applicationVersion, ApplicationVersion applicationVersion2) {
		return APPLICATION_VERSION_COMPARATOR.compare(applicationVersion2, applicationVersion) > 0;
	}

}
