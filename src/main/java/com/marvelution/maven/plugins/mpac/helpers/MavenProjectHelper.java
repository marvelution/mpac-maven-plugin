/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.helpers;

import com.atlassian.upm.api.util.Option;
import org.apache.maven.project.MavenProject;

import java.net.URI;

/**
 * {@link MavenProject} helper
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public interface MavenProjectHelper {

	/**
	 * Get the Project {@link URI}
	 *
	 * @param project the {@link MavenProject}
	 * @return the {@link URI}
	 */
	Option<URI> getProjectUri(MavenProject project);

	/**
	 * Get the Project SCM {@link URI}
	 *
	 * @param project the {@link MavenProject}
	 * @return the {@link URI}
	 */
	Option<URI> getScmUri(MavenProject project);

	/**
	 * Get the Project CI Server {@link URI}
	 *
	 * @param project the {@link MavenProject}
	 * @return the {@link URI}
	 */
	Option<URI> getCiManagementUri(MavenProject project);

	/**
	 * Get the Project Issue Server {@link URI}
	 *
	 * @param project the {@link MavenProject}
	 * @return the {@link URI}
	 */
	Option<URI> getIssueManagementUri(MavenProject project);

}
