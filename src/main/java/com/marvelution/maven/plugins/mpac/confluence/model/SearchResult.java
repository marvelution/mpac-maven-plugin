/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.confluence.model;

import com.atlassian.marketplace.client.model.Links;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Search result entity
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public final class SearchResult {

	@JsonProperty
	private final Links links;
	@JsonProperty
	private final String type;
	@JsonProperty
	private final Long id;
	@JsonProperty
	private final String title;

	@JsonCreator
	public SearchResult(@JsonProperty("id") Long id, @JsonProperty("link") Links links, @JsonProperty("type") String type,
	                    @JsonProperty("title") String title) {
		this.id = id;
		this.links = links;
		this.type = type;
		this.title = title;
	}

	public Long getId() {
		return id;
	}

	public Links getLinks() {
		return links;
	}

	public String getTitle() {
		return title;
	}

	public String getType() {
		return type;
	}

}
