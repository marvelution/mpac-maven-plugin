/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.confluence;

import com.atlassian.marketplace.client.HttpConfiguration;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.impl.CommonsHttpHelper;
import com.atlassian.marketplace.client.impl.EntityEncoding;
import com.atlassian.marketplace.client.impl.HttpHelper;
import com.atlassian.marketplace.client.impl.JsonEntityEncoding;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.marketplace.client.util.UriBuilder;
import com.atlassian.upm.api.util.Option;
import com.marvelution.maven.plugins.mpac.confluence.model.Page;
import com.marvelution.maven.plugins.mpac.confluence.model.SearchResult;
import com.marvelution.maven.plugins.mpac.confluence.model.SearchResults;
import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.net.URI;

/**
 * Confluence Client implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public final class ConfluenceClient {

	private final HttpHelper httpHelper;
	private final URI baseUri;
	private final EntityEncoding encoding;

	/**
	 * Constructor
	 *
	 * @param baseUri       the base {@link URI} to the Confluence instance
	 * @param configuration the {@link HttpConfiguration}
	 */
	public ConfluenceClient(URI baseUri, HttpConfiguration configuration) {
		this(baseUri, new CommonsHttpHelper(configuration, baseUri), new JsonEntityEncoding());
	}

	/**
	 * Constructor
	 *
	 * @param baseUri    the base {@link URI} to the Confluence instance
	 * @param httpHelper the {@link HttpHelper} implementation
	 * @param encoding   the {@link EntityEncoding} implementation
	 */
	public ConfluenceClient(URI baseUri, HttpHelper httpHelper, EntityEncoding encoding) {
		this.baseUri = normalizeBaseUri(baseUri).resolve("rest/prototype/latest/");
		this.httpHelper = httpHelper;
		this.encoding = encoding;
	}

	/**
	 * Helper method to normalize the given base {@link URI}
	 *
	 * @param baseUri the base {@link URI}
	 * @return the normalized {@link URI}
	 */
	private static URI normalizeBaseUri(URI baseUri) {
		URI norm = baseUri.normalize();
		if (norm.getPath().endsWith("/")) {
			return norm;
		}
		return URI.create(norm.toString() + "/");
	}

	/**
	 * Search for pages on Confluence
	 *
	 * @param spacekey the key of the Space to search within
	 * @param page     the title of the page to search for
	 * @return the {@link SearchResults}
	 * @throws MpacException in case errors communicating with Confluence
	 */
	public SearchResults search(String spacekey, String page) throws MpacException {
		URI uri = UriBuilder.fromUri(baseUri.resolve("search")).queryParam("query", page).queryParam("type",
				"page").queryParam("spaceKey", spacekey).build();
		HttpHelper.Response response = httpHelper.get(uri);
		if (errorOrEmpty(response.getStatus())) {
			throw new MpacException.ServerError(response.getStatus());
		} else {
			return decode(response.getContentStream(), SearchResults.class);
		}
	}

	/**
	 * Get a {@link Page} on Confluence by Id
	 *
	 * @param id the page id
	 * @return the {@link Page}
	 * @throws MpacException in case errors communicating with Confluence
	 */
	public Option<Page> getPage(Long id) throws MpacException {
		return getPage(UriBuilder.fromUri(baseUri.resolve("content")).path(String.valueOf(id)).build());
	}

	/**
	 * Get a {@link Page} on Confluence by Id
	 *
	 * @param result the {@link SearchResult}
	 * @return the {@link Page}
	 * @throws MpacException in case errors communicating with Confluence
	 */
	public Option<Page> getPage(SearchResult result) throws MpacException {
		for (Links.Link link : result.getLinks().getItems()) {
			if ("self".equals(link.getRel())) {
				return getPage(link.getHref());
			}
		}
		return Option.none();
	}

	/**
	 * Get a {@link Page} on Confluence by its {@link URI}
	 *
	 * @param pageUri the page {@link URI}
	 * @return the {@link Page}
	 * @throws MpacException in case errors communicating with Confluence
	 */
	private Option<Page> getPage(URI pageUri) throws MpacException {
		HttpHelper.Response response = httpHelper.get(pageUri);
		if (error(response.getStatus())) {
			return Option.none();
		} else {
			return Option.some(decode(response.getContentStream(), Page.class));
		}
	}

	/**
	 * Check if the response status is an error code
	 *
	 * @param statusCode the status code to check
	 * @return {@code true} if the status code is higher then or equal to 400
	 */
	private boolean error(int statusCode) {
		return statusCode >= 400;
	}

	/**
	 * Check if the response status is an error or empty code
	 *
	 * @param statusCode the status code to check
	 * @return {@code true} if the status code is higher then or equal to 400 or 204
	 */
	private boolean errorOrEmpty(int statusCode) {
		return (statusCode >= 400) || (statusCode == 204);
	}

	/**
	 * Decode the given {@link InputStream} to a object of the type given
	 *
	 * @param inputStream the {@link InputStream} to decode
	 * @param type        the {@link Class} type to decode the {@link InputStream} to
	 * @param <T>
	 * @return the decoded object
	 * @throws MpacException in case of decoding errors
	 */
	private <T> T decode(InputStream inputStream, Class<T> type) throws MpacException {
		try {
			return encoding.decode(inputStream, type);
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
	}

}
