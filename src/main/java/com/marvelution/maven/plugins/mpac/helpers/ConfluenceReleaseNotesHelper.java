/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.helpers;

import com.atlassian.marketplace.client.HttpConfiguration;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.RequestDecorator;
import com.atlassian.marketplace.client.model.Links;
import com.atlassian.upm.api.util.Option;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.marvelution.maven.plugins.mpac.confluence.ConfluenceClient;
import com.marvelution.maven.plugins.mpac.confluence.model.Page;
import com.marvelution.maven.plugins.mpac.confluence.model.PageBody;
import com.marvelution.maven.plugins.mpac.confluence.model.SearchResult;
import com.marvelution.maven.plugins.mpac.confluence.model.SearchResults;
import com.marvelution.maven.plugins.mpac.exceptions.HelperException;
import com.marvelution.maven.plugins.mpac.model.ReleaseNotes;
import com.marvelution.maven.plugins.mpac.model.ReleaseNotesContext;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.model.Model;
import org.apache.maven.settings.Server;
import org.apache.velocity.VelocityContext;
import org.codehaus.plexus.interpolation.*;
import org.codehaus.plexus.logging.AbstractLogEnabled;
import org.codehaus.plexus.velocity.VelocityComponent;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.StringWriter;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * {@link ReleaseNotesHelper} that uses Confluence as a source for the release notes
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public class ConfluenceReleaseNotesHelper extends AbstractLogEnabled implements ReleaseNotesHelper {

	private VelocityComponent velocity;

	@Override
	public ReleaseNotes generateReleaseNotes(HelperContext helperContext, ReleaseNotesContext releaseNotesContext) throws HelperException {
		ConfluenceClient client = getConfluenceClient(helperContext, releaseNotesContext);
		try {
			SearchResults results = client.search(releaseNotesContext.getSpaceKey(), interpolate(releaseNotesContext.getPageTitle(),
					helperContext.getProject().getModel()));
			if (results.getTotalSize() > 1) {
				throw new HelperException("Multiple release note pages found");
			} else {
				SearchResult result = results.getResults().get(0);
				Option<Page> page = client.getPage(result);
				if (page.isDefined()) {
					ReleaseNotes releaseNotes = new ReleaseNotes();
					releaseNotes.setSummary(getSummary(page.get().getBody()).getOrElse(releaseNotesContext.getSummary()));
					VelocityContext velocityContext = new VelocityContext();
					Links.Link link = Iterables.find(page.get().getLinks().getItems(), new Predicate<Links.Link>() {
						@Override
						public boolean apply(Links.Link link) {
							return (link.getType().isDefined() && "text/html".equals(link.getType().get())) && "alternate".equals(link
									.getRel());
						}
					}, null);
					releaseNotes.setLink(link);
					velocityContext.put("link", link);
					velocityContext.put("notes", parseWikiReleaseNotes(page.get().getBody()));
					try {
						StringWriter writer = new StringWriter();
						velocity.getEngine().mergeTemplate("/templates/release-notes.vm", "UTF-8", velocityContext, writer);
						releaseNotes.setNotes(writer.toString());
					} catch (Exception e) {
						throw new HelperException("Failed to process the release-notes.vm template", e);
					}
					return releaseNotes;
				} else {
					throw new HelperException("Unable to get page from " + result.getTitle());
				}
			}
		} catch (InterpolationException e) {
			throw new HelperException("Unable to interpolate the release notes page name", e);
		} catch (MpacException e) {
			throw new HelperException("Unable to locate the release notes page in Confluence", e);
		}
	}

	private String interpolate(String pageTitle, Model model) throws InterpolationException {
		if (pageTitle != null && pageTitle.contains("${")) {
			StringSearchInterpolator interpolator = new StringSearchInterpolator();
			List<String> pomPrefixes = Arrays.asList("pom.", "project.");
			interpolator.addValueSource(new PrefixedObjectValueSource(pomPrefixes, model, false));
			interpolator.addValueSource(new MapBasedValueSource(model.getProperties()));
			interpolator.addValueSource(new ObjectBasedValueSource(model));
			pageTitle = interpolator.interpolate(pageTitle, new PrefixAwareRecursionInterceptor(pomPrefixes));
		}
		return pageTitle;
	}

	/**
	 * Getter for the {@link ConfluenceClient}
	 *
	 * @param helperContext       the {@link HelperContext}
	 * @param releaseNotesContext the {@link ReleaseNotesContext}
	 * @return the {@link ConfluenceClient}
	 */
	ConfluenceClient getConfluenceClient(HelperContext helperContext, ReleaseNotesContext releaseNotesContext) {
		return new ConfluenceClient(URI.create(releaseNotesContext.getServerUri()), getHttpConfiguration(helperContext,
				releaseNotesContext.getServerId()));
	}

	/**
	 * Setter for the {@link VelocityComponent}
	 *
	 * @param velocity the {@link VelocityComponent}
	 */
	public void setVelocity(VelocityComponent velocity) {
		this.velocity = velocity;
	}

	/**
	 * Parse the given {@link PageBody} and add the panels to the given {@link ReleaseNotes}
	 *
	 * @param body the {@link PageBody} to parse
	 * @return {@link Map} of notes
	 */
	private Map<String, String> parseWikiReleaseNotes(PageBody body) {
		getLogger().debug("parsing wiki body to find panel macros");
		getLogger().debug(body.getValue());
		Map<String, String> notes = Maps.newLinkedHashMap();
		Document doc = Jsoup.parse(body.getValue().replaceAll("ac:", "ac"));
		for (Element panel : doc.select("acstructured-macro[acname=panel]")) {
			getLogger().debug("located a panel macro");
			Element title = panel.select("acparameter[acname=title]").first();
			if (StringUtils.isNotBlank(title.text())) {
				getLogger().debug("Adding panel '" + title.text() + "' to the release notes");
				Element value = panel.select("acrich-text-body").first();
				notes.put(title.text(), value.html());
			}
		}
		return notes;
	}

	/**
	 * Get the release notes summary from the wiki page
	 *
	 * @param body the {@link PageBody}
	 * @return the summary, may be {@code null}
	 */
	private Option<String> getSummary(PageBody body) {
		Document doc = Jsoup.parse(body.getValue());
		for (Element element : doc.body().getAllElements()) {
			if (element.tagName().matches("[hH{1}][0-9{1}]") && StringUtils.isNotBlank(element.text())) {
				return Option.some(element.text());
			}
		}
		return Option.none();
	}

	/**
	 * Get the {@link HttpConfiguration}
	 *
	 * @param helperContext the {@link HelperContext}
	 * @param serverId      the server Id of the credentials to configure
	 * @return the {@link HttpConfiguration}
	 */
	private HttpConfiguration getHttpConfiguration(HelperContext helperContext, String serverId) {
		HttpConfiguration.Builder builder = HttpConfiguration.builder();
		Server server = helperContext.getSettings().getServer(serverId);
		if (server != null) {
			builder.username(server.getUsername());
			builder.password(server.getPassword());
		}
		builder.requestDecorator(new RequestDecorator() {
			@Override
			public Map<String, String> getRequestHeaders() {
				return ImmutableMap.of("Accept", "application/json");
			}
		});
		return builder.build();
	}

}
