/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.model.Plugin;
import com.atlassian.upm.api.util.Option;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Simple MOJO that log a line if the plugin is located
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
@Mojo(name = "plugin-details", aggregator = false, requiresProject = true, threadSafe = true)
public class PluginDetailsMpacMojo extends AbstractMpacMojo {

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
			Option<Plugin> plugin = getPlugin();
			if (plugin.isDefined()) {
				getLog().info("Located plugin with key: " + plugin.get().getPluginKey() + " by " + plugin.get().getVendor().get().getName
						());
			} else {
				throw new MojoFailureException("No plugin exists with key: " + pluginKey);
			}
		} catch (MpacException e) {
			throw new MojoExecutionException(e.getMessage(), e);
		}
	}

}
