/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.model;

import com.atlassian.marketplace.client.model.Links;
import com.atlassian.upm.api.util.Option;
import org.apache.commons.lang.StringUtils;

/**
 * Release Notes
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public class ReleaseNotes {

	private Links.Link link;
	private String summary;
	private String notes;

	public Option<Links.Link> getLink() {
		if (link == null) {
			return Option.none();
		} else {
			return Option.some(link);
		}
	}

	public void setLink(Links.Link link) {
		this.link = link;
	}

	public Option<String> getSummary() {
		if (StringUtils.isBlank(summary)) {
			return Option.none();
		} else {
			return Option.some(summary);
		}
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public Option<String> getNotes() {
		if (StringUtils.isBlank(notes)) {
			return Option.none();
		} else {
			return Option.some(notes);
		}
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

}
