/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.helpers;

import com.atlassian.upm.api.util.Option;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.logging.AbstractLogEnabled;

import java.net.URI;

/**
 * Default {@link MavenProjectHelper} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public class DefaultMavenProjectHelper extends AbstractLogEnabled implements MavenProjectHelper {

	@Override
	public Option<URI> getProjectUri(MavenProject project) {
		if (StringUtils.isNotBlank(project.getUrl())) {
			return Option.some(URI.create(project.getUrl()));
		}
		return Option.none();
	}

	@Override
	public Option<URI> getScmUri(MavenProject project) {
		if (project.getScm() != null && StringUtils.isNotBlank(project.getScm().getUrl())) {
			return Option.some(URI.create(project.getScm().getUrl()));
		}
		return Option.none();
	}

	@Override
	public Option<URI> getCiManagementUri(MavenProject project) {
		if (project.getCiManagement() != null && StringUtils.isNotBlank(project.getCiManagement().getUrl())) {
			return Option.some(URI.create(project.getCiManagement().getUrl()));
		}
		return Option.none();
	}

	@Override
	public Option<URI> getIssueManagementUri(MavenProject project) {
		if (project.getIssueManagement() != null && StringUtils.isNotBlank(project.getIssueManagement().getUrl())) {
			return Option.some(URI.create(project.getIssueManagement().getUrl()));
		}
		return Option.none();
	}

}
