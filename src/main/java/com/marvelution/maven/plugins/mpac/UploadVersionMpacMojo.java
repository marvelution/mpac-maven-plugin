/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.ApplicationDetailQuery;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.PluginVersionUpdate;
import com.atlassian.marketplace.client.model.*;
import com.atlassian.upm.api.util.Option;
import com.google.common.collect.Sets;
import com.marvelution.maven.plugins.mpac.exceptions.HelperException;
import com.marvelution.maven.plugins.mpac.helpers.HelperContext;
import com.marvelution.maven.plugins.mpac.helpers.ReleaseNotesHelper;
import com.marvelution.maven.plugins.mpac.model.ReleaseNotes;
import com.marvelution.maven.plugins.mpac.model.ReleaseNotesContext;
import com.marvelution.maven.plugins.mpac.utils.ApplicationVersionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.versioning.InvalidVersionSpecificationException;
import org.apache.maven.artifact.versioning.Restriction;
import org.apache.maven.artifact.versioning.VersionRange;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * MOJO to upload a new version for a plugin
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
@Mojo(name = "upload-version", aggregator = false, requiresProject = true, threadSafe = true, defaultPhase = LifecyclePhase.DEPLOY)
public class UploadVersionMpacMojo extends AbstractMpacMojo {

	@Component
	private Map<String, ReleaseNotesHelper> releaseNotesHelpers;

	@Parameter(defaultValue = "${releaseNotesContext}")
	private ReleaseNotesContext releaseNotesContext;
	@Parameter(defaultValue = "${applicationCompatibilities}")
	private Map<String, String> applicationCompatibilities;
	@Parameter(property = "licenseIdentifier", required = true)
	private LicenseIdentifier licenseIdentifier;
	@Parameter(property = "supportStatus", defaultValue = "UNSUPPORTED")
	private SupportStatus supportStatus;
	@Parameter(property = "marketplaceType", defaultValue = "FREE")
	private MarketplaceType marketplaceType;
	@Parameter(property = "addonType", defaultValue = "PLUGINS_2")
	private AddonType addonType;
	@Parameter(property = "stable", defaultValue = "true")
	private boolean stable;
	@Parameter(property = "buildNumber", required = true)
	private long buildNumber;
	@Parameter(property = "published", defaultValue = "false")
	private boolean published;
	@Parameter(property = "youtubeId", required = false)
	private String youtubeId;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
			Option<Plugin> plugin = getPlugin();
			if (!plugin.isDefined()) {
				throw new MojoExecutionException("No plugin with key " + pluginKey + " from the Marketplace, " +
						"please create it first and try again");
			}
			if (plugin.get().getVersion().getBuildNumber() > buildNumber) {
				getLog().warn("Found newer build-number [" + plugin.get().getVersion().getBuildNumber() + "] on the Marketplace...");
				long oldBuildNumber = buildNumber;
				buildNumber = plugin.get().getVersion().getBuildNumber() + 1;
				getLog().warn("Using build-number [" + buildNumber + "] instead of [" + oldBuildNumber + "]");
			}
		} catch (MpacException e) {
			throw new MojoExecutionException("Failed to get the plugin " + pluginKey + " from the Marketplace", e);
		}
		final HelperContext helperContext = getHelperContext();
		Artifact artifact = getArtifact();
		PluginVersionUpdate.Builder versionUpdate = PluginVersionUpdate.newVersion(pluginKey, buildNumber, project.getVersion(),
				PluginVersionUpdate.Deployment.deployableFromFile(artifact.getFile()), licenseIdentifier);
		versionUpdate.published(published).stable(stable).supportStatus(supportStatus).marketplaceType(marketplaceType);
		versionUpdate.addonType(addonType).releaseDate(new Date());
		if (StringUtils.isNotBlank(youtubeId)) {
			versionUpdate.youtubeId(Option.some(youtubeId));
		}
		try {
			ReleaseNotes notes = getReleaseNotesHelper().generateReleaseNotes(helperContext, releaseNotesContext);
			versionUpdate.releaseNotes(notes.getNotes());
			if (notes.getSummary().isDefined()) {
				versionUpdate.summary(notes.getSummary());
			} else {
				throw new MojoExecutionException("No summary provided by plugin configuration or release notes!");
			}
		} catch (HelperException e) {
			throw new MojoExecutionException("Failed to generate the release notes: " + e.getMessage(), e);
		}
		addLinks(versionUpdate);
		addCompatibilities(versionUpdate);
		try {
			if (!interactWithMarketplace()) {
				// Strip the deployment as we don't want to upload it
				versionUpdate.deployment(PluginVersionUpdate.Deployment.nonDeployable(artifact.getFile().toURI()));
			}
			getMarketplaceClient().plugins().putVersion(versionUpdate.build());
		} catch (MpacException e) {
			if (interactWithMarketplace()) {
				throw new MojoExecutionException("Failed to upload the new version", e);
			}
		}
	}

	/**
	 * Add all the compatibilities to the Version Update
	 *
	 * @param versionUpdate the {@link com.atlassian.marketplace.client.api.PluginVersionUpdate.Builder}
	 */
	void addCompatibilities(PluginVersionUpdate.Builder versionUpdate) throws MojoExecutionException {
		Set<ApplicationKey> applicationKeys = Sets.newHashSet();
		for (Map.Entry<String, String> entry : applicationCompatibilities.entrySet()) {
			ApplicationKey appKey = ApplicationKey.valueOf(entry.getKey());
			if (applicationKeys.contains(appKey)) {
				throw new MojoExecutionException("Duplicate application compatibility configured for " + appKey.getKey() +
						"! Please merge these and try again");
			}
			applicationKeys.add(appKey);
			try {
				VersionRange versionRange = VersionRange.createFromVersionSpec(entry.getValue());
				Option<Application> app = getMarketplaceClient().applications().get(ApplicationDetailQuery.builder(appKey).limitVersions
						(Option.<Integer>none()).build());
				if (app.isDefined()) {
					List<ApplicationVersion> applicationVersions = ApplicationVersionUtils.getSortedApplicationVersions(app.get());
					ApplicationVersion min = null, max = null;
					getLog().debug("Resolving version range '" + versionRange.toString() + "' to " + appKey.getName() + " versions");
					if (versionRange.getRecommendedVersion() != null) {
						// Single version range
						min = ApplicationVersionUtils.getApplicationVersion(applicationVersions, versionRange.getRecommendedVersion());
					} else if (versionRange.getRestrictions().size() == 1) {
						// Multiple versions range
						for (Restriction restriction : versionRange.getRestrictions()) {
							ApplicationVersion lower = restriction.getLowerBound() != null ? ApplicationVersionUtils.getApplicationVersion
									(applicationVersions, restriction.getLowerBound()) : applicationVersions.get(0);
							if (!restriction.isLowerBoundInclusive()) {
								lower = ApplicationVersionUtils.getNextApplicationVersion(applicationVersions, lower);
							}
							ApplicationVersion upper = restriction.getUpperBound() != null ? ApplicationVersionUtils.getApplicationVersion
										(applicationVersions, restriction.getUpperBound()) :  applicationVersions.get(applicationVersions
										.size() - 1);
							if (!restriction.isUpperBoundInclusive()) {
								upper = ApplicationVersionUtils.getPreviousApplicationVersion(applicationVersions, upper);
							}
							if (min == null || ApplicationVersionUtils.isOlder(min, lower)) {
								min = lower;
							}
							if (max == null || ApplicationVersionUtils.isNewer(max, upper)) {
								max = upper;
							}
						}
					} else {
						throw new MojoExecutionException("Unsupported version range restriction.");
					}
					if (min != null && max != null) {
						getLog().info("Adding compatibility to " + appKey.getName() + " " + min.getVersion() + " - " + max.getVersion());
						versionUpdate.compatibility(appKey, min.getVersion(), max.getVersion());
					} else if (min != null) {
						getLog().info("Adding compatibility to " + appKey.getName() + " " + min.getVersion());
						versionUpdate.compatibility(appKey, min.getVersion(), min.getVersion());
					} else {
						throw new MojoExecutionException("Unable to find minimum version of " + appKey.getName() + " with " +
								"version range " + versionRange.toString());
					}
				} else {
					throw new MojoExecutionException("Unsupported application [" + appKey.getKey() + "]");
				}
			} catch (MpacException e) {
				throw new MojoExecutionException("Failed to get the application definition for " + appKey, e);
			} catch (InvalidVersionSpecificationException e) {
				throw new MojoExecutionException("Invalid version range [" + entry.getValue() + "] for application [" + appKey.getKey() +
						"]", e);
			}
		}
	}

	/**
	 * Add all the project links to the Version Update
	 *
	 * @param versionUpdate the {@link PluginVersionUpdate.Builder}
	 */
	void addLinks(PluginVersionUpdate.Builder versionUpdate) {
		Option<URI> projectUri = projectHelper.getProjectUri(project);
		if (projectUri.isDefined()) {
			versionUpdate.addLink("documentation", projectUri.get());
			versionUpdate.addLink("learn-more", projectUri.get());
			versionUpdate.addLink("wiki", projectUri.get());
		}
		Option<URI> scmUri = projectHelper.getScmUri(project);
		if (scmUri.isDefined()) {
			versionUpdate.addLink("source", scmUri.get());
		}
		Option<URI> ciUri = projectHelper.getCiManagementUri(project);
		if (ciUri.isDefined()) {
			versionUpdate.addLink("source", ciUri.get());
		}
		Option<URI> issueUri = projectHelper.getIssueManagementUri(project);
		if (issueUri.isDefined()) {
			versionUpdate.addLink("issue-tracker", issueUri.get());
		}
	}

	/**
	 * Get the main {@link Artifact} for the current project
	 *
	 * @return the {@link Artifact}
	 */
	private Artifact getArtifact() {
		return project.getArtifact();
	}

	/**
	 * Getter for the {@link ReleaseNotesHelper} by the {@link ReleaseNotesContext#source} configured
	 *
	 * @return the {@link ReleaseNotesHelper}
	 * @throws MojoExecutionException in case the wanted {@link ReleaseNotesHelper} is unknown
	 */
	private ReleaseNotesHelper getReleaseNotesHelper() throws MojoExecutionException {
		if (releaseNotesHelpers.containsKey(releaseNotesContext.getSource())) {
			return releaseNotesHelpers.get(releaseNotesContext.getSource());
		} else {
			throw new MojoExecutionException("Unknown release notes helper source " + releaseNotesContext.getSource());
		}
	}

}
