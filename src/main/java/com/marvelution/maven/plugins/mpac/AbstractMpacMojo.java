/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac;

import com.atlassian.marketplace.client.HttpConfiguration;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.PluginDetailQuery;
import com.atlassian.marketplace.client.impl.CommonsHttpHelper;
import com.atlassian.marketplace.client.impl.DefaultMarketplaceClient;
import com.atlassian.marketplace.client.impl.EntityEncoding;
import com.atlassian.marketplace.client.model.Plugin;
import com.atlassian.upm.api.util.Option;
import com.marvelution.maven.plugins.mpac.helpers.DelegatingHttpHelper;
import com.marvelution.maven.plugins.mpac.helpers.HelperContext;
import com.marvelution.maven.plugins.mpac.helpers.MavenProjectHelper;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.codehaus.plexus.logging.LoggerManager;

import java.io.File;
import java.net.URI;

/**
 * Base MOJO for the MPAC MOJOs
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class AbstractMpacMojo extends AbstractMojo {

	private MarketplaceClient client;

	@Component
	private LoggerManager loggerManager;
	@Component
	protected Settings settings;
	@Component
	protected MavenProject project;
	@Component
	protected MavenSession session;
	@Component
	protected EntityEncoding encoding;
	@Component
	protected MavenProjectHelper projectHelper;

	@Parameter(defaultValue = "${basedir}", readonly = true, required = true)
	protected File basedir;
	@Parameter(property = "pluginKey", required = true)
	protected String pluginKey;
	@Parameter(property = "serverUri", defaultValue = "https://marketplace.atlassian.com", required = true)
	private URI serverUri;
	@Parameter(property = "serverId", required = false)
	private String serverId;
	@Parameter(property = "offline", defaultValue = "${settings.offline}")
	protected boolean offline;
	@Parameter(property = "localOnly", defaultValue = "false")
	private boolean localOnly;

	/**
	 * Get the {@link MarketplaceClient} for the {@link #serverUri}.
	 * Credentials are taken from the provided {@link #serverId} in the {@link Settings}
	 *
	 * @return the {@link MarketplaceClient}
	 */
	protected MarketplaceClient getMarketplaceClient() {
		if (client == null) {
			HttpConfiguration.Builder builder = HttpConfiguration.builder();
			final Server server = settings.getServer(serverId);
			if (server != null) {
				builder.username(server.getUsername()).password(server.getPassword());
			}
			DelegatingHttpHelper httpHelper = new DelegatingHttpHelper(new CommonsHttpHelper(builder.build(), serverUri), offline, localOnly);
			httpHelper.enableLogging(loggerManager.getLoggerForComponent(DelegatingHttpHelper.class.getName()));
			client = new DefaultMarketplaceClient(serverUri, httpHelper, encoding);
		}
		return client;
	}

	/**
	 * Getter for the {@link HelperContext} for this session
	 *
	 * @return the {@link HelperContext}
	 */
	protected HelperContext getHelperContext() {
		return HelperContext.builder().session(session).settings(settings).project(project).basedir(basedir).build();
	}

	/**
	 * Get if interaction with the Marketplace is allowed
	 *
	 * @return {@code true} or {@code false}
	 */
	protected boolean interactWithMarketplace() {
		return !offline && !localOnly;
	}

	/**
	 * Get the {@link Plugin} from the Marketplace
	 *
	 * @return the {@link Plugin}
	 * @throws MpacException in case of errors
	 */
	protected Option<Plugin> getPlugin() throws MpacException {
		return getMarketplaceClient().plugins().get(PluginDetailQuery.builder(pluginKey).build());
	}

}
