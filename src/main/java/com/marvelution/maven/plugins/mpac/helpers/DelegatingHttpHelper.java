/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.helpers;

import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.impl.HttpHelper;
import com.google.common.collect.Multimap;
import org.codehaus.plexus.logging.AbstractLogEnabled;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;

/**
 * Delegating {@link HttpHelper}
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public class DelegatingHttpHelper extends AbstractLogEnabled implements HttpHelper {

	private final HttpHelper delegate;
	private final boolean offline;
	private final boolean localOnly;

	public DelegatingHttpHelper(HttpHelper delegate, boolean offline, boolean localOnly) {
		this.delegate = delegate;
		this.offline = offline;
		this.localOnly = localOnly;
	}

	@Override
	public Response get(URI uri) throws MpacException {
		getLogger().debug("Delegating GET " + uri.toASCIIString());
		return delegate.get(uri);
	}

	@Override
	public Response put(URI uri, byte[] bytes) throws MpacException {
		if (delegate()) {
			getLogger().debug("Delegating PUT " + uri.toASCIIString());
			return delegate.put(uri, bytes);
		} else {
			getLogger().warn("Shielding PUT " + uri.toASCIIString());
			getLogger().debug("PUT Data: " + new String(bytes));
			return new ResponseImpl();
		}
	}

	@Override
	public Response post(URI uri, InputStream inputStream, long l, String s, String s2) throws MpacException {
		if (delegate()) {
			getLogger().debug("Delegating POST " + uri.toASCIIString());
			return delegate.post(uri, inputStream, l, s, s2);
		} else {
			getLogger().warn("Shielding POST " + uri.toASCIIString());
			return new ResponseImpl();
		}
	}

	@Override
	public Response postParams(URI uri, Multimap<String, String> stringStringMultimap) throws MpacException {
		if (delegate()) {
			getLogger().debug("Delegating POST " + uri.toASCIIString());
			return delegate.postParams(uri, stringStringMultimap);
		} else {
			getLogger().warn("Shielding POST " + uri.toASCIIString());
			return new ResponseImpl();
		}
	}

	@Override
	public Response delete(URI uri) throws MpacException {
		if (delegate()) {
			getLogger().debug("Delegating DELETE " + uri.toASCIIString());
			return delegate.delete(uri);
		} else {
			getLogger().warn("Shielding DELETE " + uri.toASCIIString());
			return new ResponseImpl();
		}
	}

	private boolean delegate() {
		return !localOnly && !offline;
	}

	private static class ResponseImpl implements Response {

		@Override
		public int getStatus() {
			return 406;
		}

		@Override
		public InputStream getContentStream() throws MpacException {
			return new ByteArrayInputStream(new byte[0]);
		}

		@Override
		public boolean isEmpty() {
			return true;
		}

		@Override
		public void close() {}

	}

}
