/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac.confluence;

import com.atlassian.marketplace.client.HttpConfiguration;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.RequestDecorator;
import com.atlassian.upm.api.util.Option;
import com.google.common.collect.ImmutableMap;
import com.marvelution.maven.plugins.mpac.confluence.model.Page;
import com.marvelution.maven.plugins.mpac.confluence.model.PageBody;
import com.marvelution.maven.plugins.mpac.confluence.model.SearchResult;
import com.marvelution.maven.plugins.mpac.confluence.model.SearchResults;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URI;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Integration test for the {@link ConfluenceClient}
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public class ConfluenceClientIT {

	public static final String SPACEKEY = "MMP";
	private static URI baseUri;
	private static HttpConfiguration configuration;
	private ConfluenceClient client;

	@BeforeClass
	public static void setupClass() {
		baseUri = URI.create("http://localhost:1990/confluence");
		configuration = HttpConfiguration.builder().username("admin").password("admin").requestDecorator(new RequestDecorator() {
			@Override
			public Map<String, String> getRequestHeaders() {
				return ImmutableMap.of("Accept", "application/json");
			}
		}).build();
	}

	@Before
	public void setup() {
		client = new ConfluenceClient(baseUri, configuration);
	}

	@Test
	public void testSearch() throws MpacException {
		SearchResults results = client.search(SPACEKEY, "1.2.3");
		assertThat(results.getTotalSize(), is(1));
		SearchResult result = results.getResults().get(0);
		assertThat(result, notNullValue());
		assertThat(result.getId(), is(950274L));
		assertThat(result.getType(), is("page"));
		assertThat(result.getTitle(), is("1.2.3"));
	}

	@Test
	public void testGetPageById() throws MpacException {
		validate(client.getPage(950274L));
	}

	@Test
	public void testGetPageBySearchResult() throws MpacException {
		SearchResults results = client.search(SPACEKEY, "1.2.3");
		assertThat(results.getTotalSize(), is(1));
		validate(client.getPage(results.getResults().get(0)));
	}

	private void validate(Option<Page> page) {
		assertThat(page.isDefined(), is(true));
		assertThat(page.get().getId(), is(950274L));
		assertThat(page.get().getType(), is("page"));
		assertThat(page.get().getTitle(), is("1.2.3"));
		assertThat(page.get().getWikiLink(), is("[MMP:1.2.3]"));
		assertThat(page.get().getBody(), notNullValue());
		PageBody body = page.get().getBody();
		assertThat(body.getType(), is(2));
		assertThat(body.getValue(), notNullValue());
	}

}
