/*
 * MPAC Maven Plugin
 * Copyright (C) 2013 Marvelution
 * info@marvelution.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.marvelution.maven.plugins.mpac;

import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.PluginVersionUpdate;
import com.atlassian.marketplace.client.model.LicenseIdentifier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.project.DefaultProjectBuildingRequest;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.ProjectBuilder;

import java.io.File;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Test case for the {@link UploadVersionMpacMojo}
 *
 * @author Mark Rekveld
 * @since 1.0.0-alpha-1
 */
public class UploadVersionMpacMojoTest extends AbstractMojoTestCase {

	private ProjectBuilder projectBuilder;

	@Override
	public void setUp() throws Exception {
		super.setUp();
		projectBuilder = lookup(ProjectBuilder.class);
	}

	/**
	 * Test the version range transformation to {@link PluginVersionUpdate.Compatibility}
	 *
	 * @throws Exception in case of test failures
	 */
	public void testAddCompatibilities() throws Exception {
		File testPom = new File(getBasedir(), "src/test/resources/projects/compatibility-test/pom.xml");
		MavenProject project = projectBuilder.build(testPom, new DefaultProjectBuildingRequest()).getProject();
		UploadVersionMpacMojo mojo = (UploadVersionMpacMojo) lookupConfiguredMojo(project, "upload-version");
		assertThat(mojo, notNullValue());
		PluginVersionUpdate.Builder builder = PluginVersionUpdate.newVersion("dummy", 1L, "1.0",
				PluginVersionUpdate.Deployment.nonDeployable(testPom.toURI()), LicenseIdentifier.ASL);
		mojo.addCompatibilities(builder);
		PluginVersionUpdate update = builder.build();
		List<PluginVersionUpdate.Compatibility> compatibilities = Lists.newArrayList(
				new PluginVersionUpdate.Compatibility(ApplicationKey.BAMBOO, "5.0", "5.0"),
				new PluginVersionUpdate.Compatibility(ApplicationKey.CONFLUENCE, "5.1", "5.2.5"),
				new PluginVersionUpdate.Compatibility(ApplicationKey.FISHEYE, "1.6.1", "2.5.0"),
				new PluginVersionUpdate.Compatibility(ApplicationKey.JIRA, "5.2", "6.1.1"),
				new PluginVersionUpdate.Compatibility(ApplicationKey.STASH, "2.0.0", "2.5.4")
		);
		assertThat(Iterables.size(update.getCompatibilities()), is(compatibilities.size()));
		for (PluginVersionUpdate.Compatibility compatibility : update.getCompatibilities()) {
			assertThat(compatibilities.contains(compatibility), is(true));
		}
	}

}
